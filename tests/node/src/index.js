const join = require("lodash/join");

function component(arr) {
  const element = document.createElement("div");

  element.innerHTML = join(arr, " ");

  return element;
}

document.body.appendChild(component(["Panic", "Lobster"]));

module.exports = { component };
