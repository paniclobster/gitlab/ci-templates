/* global expect it */

require("@testing-library/jest-dom/extend-expect");

const { component } = require("../index");

it("renders without error", () => {
  const div = document.createElement("div");

  div.appendChild(component(["Panic", "Lobster"]));

  expect(div).toHaveTextContent("Panic Lobster");
});
