package main

import (
	"testing"
)

func TestReturnTrue(t *testing.T) {
	got := ReturnTrue()

	if got != true {
		t.Errorf("got: %t; want %t", got, true)
	}
}
